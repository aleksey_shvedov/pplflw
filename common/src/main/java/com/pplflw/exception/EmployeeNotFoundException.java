package com.pplflw.exception;

public class EmployeeNotFoundException  extends RuntimeException{

    public EmployeeNotFoundException(String message) {
        super(message);
    }

    public EmployeeNotFoundException(Throwable ex) {
        super(ex);
    }

    public EmployeeNotFoundException(String message, Throwable ex) {
        super(message, ex);
    }
}
