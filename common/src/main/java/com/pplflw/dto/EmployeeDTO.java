package com.pplflw.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {

    private Long id;
    @ApiModelProperty(required = true)
    @NotNull
    private String name;
    @ApiModelProperty(required = true)
    @NotNull
    private int age;
    @ApiModelProperty(required = true)
    @NotNull
    private String address;
    private EmployeeState state;



}
