package com.pplflw.dto;

public enum EmployeeState {
    NEW,
    ADDED,
    IN_CHECK,
    APPROVED,
    ACTIVE
}
