package com.pplflw;


import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.statemachine.util.GuardUtil;
import com.pplflw.statemachine.persist.EmployeeStateMachinePersister;
import com.pplflw.statemachine.service.StateServiceImpl;
import com.pplflw.statemachine.state.EventState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
public class StateServiceTest {

    @InjectMocks
    private StateServiceImpl stateService;

    @Mock
    private StateMachineFactory<EmployeeState, EventState> stateMachineFactory;

    @Mock
    private StateMachinePersister<EmployeeState, EventState, String> stateMachinePersister;

    @Mock
    private EmployeeStateMachinePersister employeeStateMachinePersister;

    @Mock
    private GuardUtil guardUtil;

    @Test
    public void stateServiceTest() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();
        StateMachine<EmployeeState, EventState> stateMachine = mock(StateMachine.class);
        when(stateMachinePersister.restore(any(),any())).thenReturn(stateMachine);
        stateService.act(employeeDTO);

        verify(stateMachinePersister, times(1)).persist(any(),any());

    }

}
