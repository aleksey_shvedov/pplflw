package com.pplflw;


import com.pplflw.dao.EmployeeDAO;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.entity.Employee;
import com.pplflw.statemachine.guard.EmployeeCheckGuard;
import com.pplflw.statemachine.util.GuardUtil;
import com.pplflw.statemachine.state.EventState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.statemachine.StateContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class EmployeeGuardServiceTest {

    @InjectMocks
    private EmployeeCheckGuard checkGuard;

    @Mock
    private GuardUtil guardUtil;

    @Mock
    private EmployeeDAO employeeDAO;

    @Test
    public void guardTest() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();

        when(guardUtil.getEmployee(any())).thenReturn(employeeDTO);
        when(employeeDAO.findById(any())).thenReturn(Optional.of(Employee.builder().build()));


        StateContext<EmployeeState, EventState> stateContext = mock(StateContext.class);

        assertTrue(checkGuard.evaluate(stateContext));

    }

    @Test
    public void guardTestEmployeeNotInContext() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();

        when(guardUtil.getEmployee(any())).thenReturn(null);
        StateContext<EmployeeState, EventState> stateContext = mock(StateContext.class);
        assertFalse(checkGuard.evaluate(stateContext));

    }

}
