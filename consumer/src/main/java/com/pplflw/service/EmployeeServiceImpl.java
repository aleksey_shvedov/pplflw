package com.pplflw.service;

import com.pplflw.dao.EmployeeDAO;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.entity.Employee;
import com.pplflw.exception.EmployeeNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import org.slf4j.Logger;

@Component
@Slf4j
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeDAO employeeDAO;

    @Override
    public void updateState(Long id, EmployeeState state) {
        log.info("Update state of employee: {} to {} ",id,state);
        Employee employee = employeeDAO.findById(id).get();
        employee.setState(state);
        employeeDAO.save(employee);
    }

    @Override
    public EmployeeDTO getEmployeeById(long employeeId) {
        log.info("Getting employee by id: {}",employeeId);
        Optional<Employee> optionalEmployee = employeeDAO.findById(employeeId);
        if (optionalEmployee.isPresent()){
            Employee employee = optionalEmployee.get();
            return EmployeeDTO.builder()
                    .id(employee.getId())
                    .name(employee.getName())
                    .age(employee.getAge())
                    .address(employee.getAddress())
                    .state(employee.getState())
                    .build();
        }
        throw new EmployeeNotFoundException("There is no employee with such id : "+employeeId);
    }
}
