package com.pplflw.service;

import com.pplflw.dto.EmployeeDTO;
import com.pplflw.statemachine.service.StateService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaListenerImpl {


    @Autowired
    private StateService stateService;

    @KafkaListener(id = "test", topics = "${kafka.topic}")
    public void consume(EmployeeDTO dto) {
        log.info("Gor message from kafka for employee: {}",dto.getId());
        stateService.act(dto);
    }


}
