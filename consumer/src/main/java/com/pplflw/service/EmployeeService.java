package com.pplflw.service;

import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;

public interface EmployeeService {

    void updateState(Long id, EmployeeState active);

    EmployeeDTO getEmployeeById(long employeeId);
}
