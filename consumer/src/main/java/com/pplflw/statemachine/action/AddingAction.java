package com.pplflw.statemachine.action;

import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.entity.Employee;
import com.pplflw.service.EmployeeService;
import com.pplflw.statemachine.state.EventState;
import com.pplflw.statemachine.util.GuardUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

@Component
public class AddingAction implements Action<EmployeeState, EventState>{

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private GuardUtil util;

    @Override
    public void execute(StateContext<EmployeeState, EventState> stateContext) {
        EmployeeDTO employeeDTO = util.getEmployee(stateContext.getStateMachine());
        Employee employee = Employee.builder()
                .id(employeeDTO.getId())
                .address(employeeDTO.getAddress())
                .age(employeeDTO.getAge())
                .name(employeeDTO.getName())
                .build();
        employeeService.updateState(employee.getId(),EmployeeState.ADDED);
    }
}
