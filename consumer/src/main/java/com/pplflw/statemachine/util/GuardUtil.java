package com.pplflw.statemachine.util;

import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.statemachine.state.EventState;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;

@Component
public class GuardUtil {
    private static final String EMPLOYEE_OBJECT = "employee";

    public EmployeeDTO getEmployee(StateMachine<EmployeeState, EventState> stateMachine) {
        Object employee = stateMachine.getExtendedState().getVariables().get(EMPLOYEE_OBJECT);
        return (EmployeeDTO) employee;
    }
    public void setEmployee(StateMachine<EmployeeState, EventState> stateMachine, EmployeeDTO employee) {
        stateMachine.getExtendedState().getVariables().put(EMPLOYEE_OBJECT, employee);
    }

}
