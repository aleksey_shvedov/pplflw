package com.pplflw.statemachine.persist;

import com.pplflw.dto.EmployeeState;
import com.pplflw.statemachine.state.EventState;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class EmployeeStateMachinePersister implements StateMachinePersist<EmployeeState, EventState, String> {

    private final HashMap<String, StateMachineContext<EmployeeState, EventState>> contexts = new HashMap<>();

    @Override
    public void write(final StateMachineContext<EmployeeState, EventState> context, String contextObj) {
        contexts.put(contextObj, context);
    }

    @Override
    public StateMachineContext<EmployeeState, EventState> read(final String contextObj) {
        return contexts.get(contextObj);
    }
}
