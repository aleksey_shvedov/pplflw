package com.pplflw.statemachine.state;

public enum EventState {

    ADDING,
    CHECKING,
    COMPLETE_CHECKING,
    ACTIVATING

}
