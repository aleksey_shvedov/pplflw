package com.pplflw.statemachine.service;

import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.statemachine.EmployeeStateEventStateMapper;
import com.pplflw.statemachine.util.GuardUtil;
import com.pplflw.statemachine.persist.EmployeeStateMachinePersister;
import com.pplflw.statemachine.state.EventState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.stereotype.Service;

@Service
public class StateServiceImpl implements StateService{

    public static final String EMPLOYEE = "employee";
    @Autowired
    private StateMachineFactory<EmployeeState, EventState> stateMachineFactory;

    @Autowired
    private StateMachinePersister<EmployeeState, EventState, String> stateMachinePersister;

    @Autowired
    private EmployeeStateMachinePersister employeeStateMachinePersister;

    @Autowired
    private GuardUtil guardUtil;

    @Override
    public void act(EmployeeDTO employee) {
        StateMachine<EmployeeState, EventState> stateMachine = stateMachineFactory.getStateMachine();
        try {
            StateMachine<EmployeeState, EventState> employeeStateMachine = stateMachinePersister.restore(stateMachine,""+employee.getId());
            guardUtil.setEmployee(employeeStateMachine,employee);
            employeeStateMachine.sendEvent(EmployeeStateEventStateMapper.getEventStateForEmployeeState(employee.getState()));
            stateMachinePersister.persist(stateMachine, employee.getId().toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
