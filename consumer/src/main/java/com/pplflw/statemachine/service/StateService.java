package com.pplflw.statemachine.service;

import com.pplflw.dto.EmployeeDTO;

public interface StateService {
    void act(EmployeeDTO employee);
}
