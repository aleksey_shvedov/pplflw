package com.pplflw.statemachine;

import com.pplflw.dto.EmployeeState;
import com.pplflw.statemachine.state.EventState;

import java.util.HashMap;
import java.util.Map;

public class EmployeeStateEventStateMapper {

    private static final Map<EmployeeState, EventState> mapping = new HashMap<>();

    static {
        mapping.put(EmployeeState.ADDED, EventState.ADDING);
        mapping.put(EmployeeState.IN_CHECK, EventState.CHECKING);
        mapping.put(EmployeeState.APPROVED, EventState.COMPLETE_CHECKING);
        mapping.put(EmployeeState.ACTIVE, EventState.ACTIVATING);
    }


    public static EventState getEventStateForEmployeeState(EmployeeState employeeState){
        if(employeeState !=null ){
            return mapping.get(employeeState);
        }else {
            return EventState.ADDING;
        }

    }
}
