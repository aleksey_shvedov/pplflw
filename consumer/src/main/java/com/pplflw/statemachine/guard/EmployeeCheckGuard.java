package com.pplflw.statemachine.guard;

import com.pplflw.dao.EmployeeDAO;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.service.EmployeeService;
import com.pplflw.statemachine.util.GuardUtil;
import com.pplflw.statemachine.state.EventState;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class EmployeeCheckGuard implements Guard<EmployeeState, EventState> {


    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private GuardUtil guardUtil;

    @Override
    public boolean evaluate(StateContext<EmployeeState, EventState> context) {
        EmployeeDTO employee = guardUtil.getEmployee(context.getStateMachine());
        log.info("Check weather  employee : {} exist",employee.getId());
        Long id = employee.getId();
        if (!Objects.nonNull(id)){
            return false;
        }
        return employeeDAO.findById(id).isPresent();

    }
}
