package com.pplflw.configuration;

import com.pplflw.dto.EmployeeState;
import com.pplflw.statemachine.action.ActivetingAction;
import com.pplflw.statemachine.action.AddingAction;
import com.pplflw.statemachine.action.CheckingAction;
import com.pplflw.statemachine.action.CompleteCheckingAction;
import com.pplflw.statemachine.guard.EmployeeCheckGuard;
import com.pplflw.statemachine.persist.EmployeeStateMachinePersister;
import com.pplflw.statemachine.state.EventState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;

import java.util.EnumSet;

import static com.pplflw.dto.EmployeeState.*;
import static com.pplflw.statemachine.state.EventState.*;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<EmployeeState, EventState> {

    @Autowired
    private AddingAction addingAction;
    @Autowired
    private CheckingAction checkingAction;
    @Autowired
    private ActivetingAction activetingAction;
    @Autowired
    private CompleteCheckingAction completeCheckingAction;
    @Autowired
    private EmployeeCheckGuard checkGuard;

    @Override
    public void configure(final StateMachineConfigurationConfigurer<EmployeeState, EventState> config) throws Exception {
        config
                .withConfiguration()
                .autoStartup(true);
    }

    @Override
    public void configure(final StateMachineStateConfigurer<EmployeeState, EventState> states) throws Exception {
        states
                .withStates()
                .initial(NEW)
                .end(ACTIVE)
                .states(EnumSet.allOf(EmployeeState.class));

    }

    @Override
    public void configure(final StateMachineTransitionConfigurer<EmployeeState, EventState> transitions) throws Exception {
        transitions
                .withExternal()
                .source(NEW)
                .target(ADDED)
                .event(ADDING)
                .guard(checkGuard)
                .action(addingAction)

                .and()
                .withExternal()
                .source(ADDED)
                .target(IN_CHECK)
                .event(CHECKING)
                .guard(checkGuard)
                .action(checkingAction)

                .and()
                .withExternal()
                .source(IN_CHECK)
                .target(APPROVED)
                .event(COMPLETE_CHECKING)
                .guard(checkGuard)
                .action(completeCheckingAction)

                .and()
                .withExternal()
                .source(APPROVED)
                .target(ACTIVE)
                .event(ACTIVATING)
                .guard(checkGuard)
                .action(activetingAction);
    }

    @Bean
    public StateMachinePersister<EmployeeState, EventState, String> persister() {
        return new DefaultStateMachinePersister<>(new EmployeeStateMachinePersister());
    }

}
