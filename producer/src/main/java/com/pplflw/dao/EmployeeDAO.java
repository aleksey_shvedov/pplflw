package com.pplflw.dao;

import com.pplflw.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeDAO extends PagingAndSortingRepository<Employee, Long> {
}
