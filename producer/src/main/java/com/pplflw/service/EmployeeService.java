package com.pplflw.service;

import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.entity.Employee;

import java.util.List;

public interface EmployeeService {
    EmployeeDTO getEmployeeById(long employeeId);

    List<Employee> getEmployees();

    EmployeeDTO addEmployee(EmployeeDTO employee);

    void changeEmployeeState(Long employeeId, EmployeeState state);
}
