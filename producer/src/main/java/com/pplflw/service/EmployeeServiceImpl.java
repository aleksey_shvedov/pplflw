package com.pplflw.service;

import com.pplflw.dao.EmployeeDAO;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.entity.Employee;
import com.pplflw.exception.EmployeeNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Value("${kafka.topic}")
    private String kafkaTopic;

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private KafkaTemplate<String, EmployeeDTO> kafkaTemplate;

    @Override
    public EmployeeDTO getEmployeeById(long employeeId) {
        log.info("getting employee : {} , by id",employeeId);
        Optional<Employee> optionalEmployee = employeeDAO.findById(employeeId);
        if (optionalEmployee.isPresent()){
            Employee employee = optionalEmployee.get();
            return EmployeeDTO.builder()
                    .id(employee.getId())
                    .name(employee.getName())
                    .age(employee.getAge())
                    .address(employee.getAddress())
                    .state(employee.getState())
                    .build();
        }
        throw new EmployeeNotFoundException("There is no employee with such id : "+employeeId);
    }

    @Override
    public List<Employee> getEmployees() {
        log.info("get all employees");
        List<Employee> employees = new ArrayList<>();
        employeeDAO.findAll().forEach(employees::add);
        return employees;
    }

    @Override
    public EmployeeDTO addEmployee(EmployeeDTO employeeDTO) {
        log.info("add new employee with name : {} ", employeeDTO.getName());
        Employee employee = Employee.builder()
                .name(employeeDTO.getName())
                .age(employeeDTO.getAge())
                .address(employeeDTO.getAddress())
                .state(EmployeeState.NEW)
                .build();
        Employee savedEmployee = employeeDAO.save(employee);
        log.info("send message to kafka for employee: {} with state ADDED ", savedEmployee.getId());
        kafkaTemplate.send(kafkaTopic, EmployeeDTO.builder()
                .id(savedEmployee.getId())
                .name(savedEmployee.getName())
                .age(savedEmployee.getAge())
                .address(savedEmployee.getAddress())
                .state(EmployeeState.ADDED).build());
        employeeDTO.setId(savedEmployee.getId());
        employeeDTO.setState(EmployeeState.NEW);
        return employeeDTO;
    }

    @Override
    public void changeEmployeeState(Long employeeId, EmployeeState state)  {
        log.info("send message to kafka for employee: {} with state {}} ", employeeId, state);
        kafkaTemplate.send(kafkaTopic, EmployeeDTO.builder()
                .id(employeeId)
                .state(state).build());
    }
}
