package com.pplflw.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class ChangeStateDTO {
    @ApiModelProperty(required = true)
    @NotNull
    private Long employeeId;
    @ApiModelProperty(required = true)
    @NotNull
    private EmployeeState state;

    public ChangeStateDTO() {
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public EmployeeState getState() {
        return state;
    }

    public void setState(EmployeeState state) {
        this.state = state;
    }

}
