package com.pplflw.controllers;

import com.pplflw.dto.ChangeStateDTO;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.entity.Employee;
import com.pplflw.exception.EmployeeNotFoundException;
import com.pplflw.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @ResponseBody
    @GetMapping(value = "/employee/{employeeId}", produces = "application/json")
    public EmployeeDTO getEmployeeById(@PathVariable("employeeId") long employeeId) {
        EmployeeDTO employee;
        try {
            employee= employeeService.getEmployeeById(employeeId);
        }catch (EmployeeNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
        return employee;
    }

    @ResponseBody
    @GetMapping(value = "/employees", produces = "application/json")
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @ResponseBody
    @PostMapping(value = "/employee", produces = "application/json")
    public EmployeeDTO addEmployee(@RequestBody @Valid EmployeeDTO employeeDto) {
        return employeeService.addEmployee(employeeDto);
    }

    @ResponseBody
    @PostMapping(value = "/employee/changeState", produces = "application/json")
    public void changeEmployeeState(@RequestBody @Valid ChangeStateDTO changeStateDTO) {
        try {
            employeeService.changeEmployeeState(changeStateDTO.getEmployeeId(),changeStateDTO.getState());
        }catch (EmployeeNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

}
