package com.pplflw;


import com.pplflw.dao.EmployeeDAO;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.dto.EmployeeState;
import com.pplflw.entity.Employee;
import com.pplflw.exception.EmployeeNotFoundException;
import com.pplflw.service.EmployeeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {


    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private KafkaTemplate kafkaTemplate;

    @Mock
    private EmployeeDAO employeeDAO;

    @Test
    public void addNewEmployeePositive() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();

        Employee employee = Employee.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();
        when(employeeDAO.save(any())).thenReturn(employee);
        EmployeeDTO addedEmployee = employeeService.addEmployee(employeeDTO);
        assertEquals(addedEmployee.getId(), employeeDTO.getId());
    }

    @Test
    public void addNewEmployeeNegative() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();

        when(employeeDAO.save(any())).thenThrow(EmployeeNotFoundException.class);

        assertThrows(EmployeeNotFoundException.class, () -> employeeService.addEmployee(employeeDTO));
    }

    @Test
    public void getEmployeePositive() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();

        Employee employee = Employee.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();
        when(employeeDAO.findById(any())).thenReturn(Optional.of(employee));
        EmployeeDTO result = employeeService.getEmployeeById(1);
        assertEquals(result.getId(), employeeDTO.getId());
    }

    @Test
    public void getEmployeeNegative() throws Exception {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();

        Employee employee = Employee.builder()
                .id(1L)
                .address("123")
                .name("123213")
                .age(1)
                .state(EmployeeState.NEW)
                .build();
        when(employeeDAO.findById(any())).thenThrow(EmployeeNotFoundException.class);
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.getEmployeeById(1));
    }

}
