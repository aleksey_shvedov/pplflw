package com.pplflw;


import com.pplflw.controllers.EmployeeController;
import com.pplflw.dto.EmployeeDTO;
import com.pplflw.exception.EmployeeNotFoundException;
import com.pplflw.service.EmployeeService;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeContollerTest {

    @Autowired
    private MockMvc mvcMock;

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private KafkaTemplate kafkaTemplate;

    @Test
    public void addNewEmployeePositiveTest() throws Exception {

        Integer id = 123;
        EmployeeDTO addedEmployee = EmployeeDTO.builder()
                .id(id.longValue())
                .name("Alex")
                .age(26)
                .address("Belarus")
                .build();
        when(employeeService.addEmployee(any(EmployeeDTO.class))).thenReturn(addedEmployee);
        mvcMock.perform(post("/employee").content("{}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Is.is(id)));

        verify(employeeService, times(1)).addEmployee(any(EmployeeDTO.class));
    }

    @Test
    public void getEmployeePositiveTest() throws Exception {

        Integer id = 123;
        EmployeeDTO employee = EmployeeDTO.builder()
                .id(id.longValue())
                .name("Alex")
                .age(26)
                .address("Belarus")
                .build();
        when(employeeService.getEmployeeById(anyLong())).thenReturn(employee);
        mvcMock.perform(get("/employee/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Is.is(id)));

        verify(employeeService, times(1)).getEmployeeById(anyLong());
    }

    @Test
    public void getEmployeeNegativeNotFoundTest() throws Exception {

        String employeeId = "123";
        when(employeeService.getEmployeeById(anyLong())).thenThrow(EmployeeNotFoundException.class);

        mvcMock.perform(get("/employee/{id}", employeeId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void sendNewStatePositiveTest() throws Exception {

        Integer id = 123;
        EmployeeDTO employee = EmployeeDTO.builder()
                .id(id.longValue())
                .name("Alex")
                .age(26)
                .address("Belarus")
                .build();
        mvcMock.perform(post("/employee/changeState").content("{}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
